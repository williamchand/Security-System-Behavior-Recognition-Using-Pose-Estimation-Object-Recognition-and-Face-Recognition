from ctypes import *
import math
import random
import os
import cv2
class darknet_recog:

    def __init__(self, thresh= 0.25, configPath = "./darknet/cfg/yolov3-tiny.cfg", weightPath = "./darknet/yolov3-tiny.weights", metaPath= "./darknet/cfg/coco.data"):
            cwd = os.path.dirname(__file__)
            winGPUdll = os.path.join(cwd, "yolo_cpp_dll.dll")
            lib = CDLL(winGPUdll, RTLD_GLOBAL)
        lib.network_width.argtypes = [c_void_p]
        lib.network_width.restype = c_int
        lib.network_height.argtypes = [c_void_p]
        lib.network_height.restype = c_int

        def network_width(net):
            return lib.network_width(net)

        def network_height(net):
            return lib.network_height(net)

        predict = lib.network_predict
        predict.argtypes = [c_void_p, POINTER(c_float)]
        predict.restype = POINTER(c_float)
        set_gpu = lib.cuda_set_device
        set_gpu.argtypes = [c_int]
        make_image = lib.make_image
        make_image.argtypes = [c_int, c_int, c_int]
        make_image.restype = DKIMAGE
        self.get_network_boxes = lib.get_network_boxes
        self.get_network_boxes.argtypes = [c_void_p, c_int, c_int, c_float, c_float, POINTER(c_int), c_int, POINTER(c_int), c_int]
        self.get_network_boxes.restype = POINTER(DKDETECTION)
        make_network_boxes = lib.make_network_boxes
        make_network_boxes.argtypes = [c_void_p]
        make_network_boxes.restype = POINTER(DKDETECTION)
        self.free_detections = lib.free_detections
        self.free_detections.argtypes = [POINTER(DKDETECTION), c_int]
        free_ptrs = lib.free_ptrs
        free_ptrs.argtypes = [POINTER(c_void_p), c_int]
        network_predict = lib.network_predict
        network_predict.argtypes = [c_void_p, POINTER(c_float)]
        reset_rnn = lib.reset_rnn
        reset_rnn.argtypes = [c_void_p]
        load_net = lib.load_network
        load_net.argtypes = [c_char_p, c_char_p, c_int]
        load_net.restype = c_void_p
        load_net_custom = lib.load_network_custom
        load_net_custom.argtypes = [c_char_p, c_char_p, c_int, c_int]
        load_net_custom.restype = c_void_p
        do_nms_obj = lib.do_nms_obj
        do_nms_obj.argtypes = [POINTER(DKDETECTION), c_int, c_int, c_float]
        self.do_nms_sort = lib.do_nms_sort
        self.do_nms_sort.argtypes = [POINTER(DKDETECTION), c_int, c_int, c_float]
        free_image = lib.free_image
        free_image.argtypes = [DKIMAGE]
        letterbox_image = lib.letterbox_image
        letterbox_image.argtypes = [DKIMAGE, c_int, c_int]
        letterbox_image.restype = DKIMAGE
        load_meta = lib.get_metadata
        lib.get_metadata.argtypes = [c_char_p]
        lib.get_metadata.restype = DKMETADATA
        load_image = lib.load_image_color
        load_image.argtypes = [c_char_p, c_int, c_int]
        load_image.restype = DKIMAGE
        rgbgr_image = lib.rgbgr_image
        rgbgr_image.argtypes = [DKIMAGE]
        self.predict_image = lib.network_predict_image
        self.predict_image.argtypes = [c_void_p, DKIMAGE]
        self.predict_image.restype = POINTER(c_float)
        
        self.netMain = load_net_custom(configPath.encode("ascii"), weightPath.encode("ascii"), 0, 1)  # batch size = 1
        self.metaMain = load_meta(metaPath.encode("ascii"))
        try:
            with open(metaPath) as metaFH:
                metaContents = metaFH.read()
                import re
                match = re.search("names *= *(.*)$", metaContents, re.IGNORECASE | re.MULTILINE)
                if match:
                    result = match.group(1)
                else:
                    result = None
                try:
                    if os.path.exists(result):
                        with open(result) as namesFH:
                            namesList = namesFH.read().strip().split("\n")
                            self.altNames = [x.strip() for x in namesList]
                except TypeError:
                    pass
        except Exception:
            pass
        
    def array_to_image(arr):
        import numpy as np
        # need to return old values to avoid python freeing memory
        arr = arr.transpose(2,0,1)
        c = arr.shape[0]
        h = arr.shape[1]
        w = arr.shape[2]
        arr = np.ascontiguousarray(arr.flat, dtype=np.float32) / 255.0
        data = arr.ctypes.data_as(POINTER(c_float))
        im = DKIMAGE(w,h,c,data)
        return im, arr

    def classify(self, net, meta, im):
        out = self.predict_image(net, im)
        res = []
        for i in range(meta.classes):
            if self.altNames is None:
                nameTag = meta.names[i]
            else:
                nameTag = self.altNames[i]
            res.append((nameTag, out[i]))
        res = sorted(res, key=lambda x: -x[1])
        return res

    def detect_image(self, net, meta, im, thresh=.5, hier_thresh=.5, nms=.45, debug= False):
        dkim, arr = darknet_recog.array_to_image(im)

        num = c_int(0)
        pnum = pointer(num)
        self.predict_image(net, dkim)
        dets = self.get_network_boxes(net, dkim.w, dkim.h, thresh, hier_thresh, None, 0, pnum, 0)
        num = pnum[0]
        if nms:
            self.do_nms_sort(dets, num, meta.classes, nms)
        res = []
        for j in range(num):
            for i in range(meta.classes):
                if dets[j].prob[i] > 0:
                    b = dets[j].bbox
                    if self.altNames is None:
                        nameTag = meta.names[i]
                    else:
                        nameTag = self.altNames[i]
                    res.append((nameTag, dets[j].prob[i], (b.x, b.y, b.w, b.h)))
        res = sorted(res, key=lambda x: -x[1])
        self.free_detections(dets, num)
        return res


    def performDetect(self, image, thresh=0.25):
        detections = self.detect_image(self.netMain, self.metaMain, image, thresh)                
        return detections
        
    def imagemarking(image, detections):
        try:
            from skimage import io, draw
            import numpy as np
            imcaption = []
            for detection in detections:
                label = detection[0]
                confidence = detection[1]
                pstring = label+": "+str(np.rint(100 * confidence))+"%"
                imcaption.append(pstring)
                bounds = detection[2]
                shape = image.shape
                yExtent = int(bounds[3])
                xEntent = int(bounds[2])
                xCoord = int(bounds[0] - bounds[2]/2)
                yCoord = int(bounds[1] - bounds[3]/2)
                boundingBox = [
                    [xCoord, yCoord],
                    [xCoord, yCoord + yExtent],
                    [xCoord + xEntent, yCoord + yExtent],
                    [xCoord + xEntent, yCoord]
                ]
                rr, cc = draw.polygon_perimeter([x[1] for x in boundingBox], [x[0] for x in boundingBox], shape= shape)
                rr2, cc2 = draw.polygon_perimeter([x[1] + 1 for x in boundingBox], [x[0] for x in boundingBox], shape= shape)
                rr3, cc3 = draw.polygon_perimeter([x[1] - 1 for x in boundingBox], [x[0] for x in boundingBox], shape= shape)
                rr4, cc4 = draw.polygon_perimeter([x[1] for x in boundingBox], [x[0] + 1 for x in boundingBox], shape= shape)
                rr5, cc5 = draw.polygon_perimeter([x[1] for x in boundingBox], [x[0] - 1 for x in boundingBox], shape= shape)
                boxColor = (int(255 * (1 - (confidence ** 2))), int(255 * (confidence ** 2)), 0)
                draw.set_color(image, (rr, cc), boxColor, alpha= 0.8)
                draw.set_color(image, (rr2, cc2), boxColor, alpha= 0.8)
                draw.set_color(image, (rr3, cc3), boxColor, alpha= 0.8)
                draw.set_color(image, (rr4, cc4), boxColor, alpha= 0.8)
                draw.set_color(image, (rr5, cc5), boxColor, alpha= 0.8)
                io.imshow(image)
                io.show()
        except Exception as e:
            print("Unable to show image: "+str(e))
            
class DKBOX(Structure):
    _fields_ = [("x", c_float),
                ("y", c_float),
                ("w", c_float),
                ("h", c_float)]

class DKDETECTION(Structure):
    _fields_ = [("bbox", DKBOX),
                ("classes", c_int),
                ("prob", POINTER(c_float)),
                ("mask", POINTER(c_float)),
                ("objectness", c_float),
                ("sort_class", c_int)]


class DKIMAGE(Structure):
    _fields_ = [("w", c_int),
                ("h", c_int),
                ("c", c_int),
                ("data", POINTER(c_float))]

class DKMETADATA(Structure):
    _fields_ = [("classes", c_int),
                ("names", POINTER(c_char_p))]

if __name__ == "__main__":
    dark = darknet_recog()
    imagePath="data/image.jpg"
    im = cv2.imread(imagePath)
    print(dark.performDetect(im))
